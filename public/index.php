<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
session_start();
header('Content-Type: text/html; charset=UTF-8');

$user = 'u20391';
$pass = '8767552';
$db = new PDO('mysql:host=localhost;dbname=u20391', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  if (!empty($_COOKIE['save'])) {
    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 1);
    setcookie('login_cheked', 1);
    setcookie('login', 1);
    setcookie('pass', '', 1);
    // Выводим сообщение пользователю.
    // Если в куках есть пароль, то выводим сообщение.
   
  }
 
  

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);

  // TODO: аналогично все поля.

 // Выдаем сообщения об ошибках.
  if ($errors['fio']) {
  setcookie('fio_error', '', 1);
  $messages[] = '<div class="">Заполните ФИО.</div>';
  }
  if ($errors['email']) {
    setcookie('email_error', '', 1);
    $messages[] = '<div class="">Заполните email.</div>';
  }
  if ($errors['date']) {
    setcookie('date_error', '', 1);
    $messages[] = '<div class="">Укажите дату рождения.</div>';
  }
  if ($errors['sex']) {
    setcookie('sex_error', '', 1);
    $messages[] = '<div class="">Выберете пол</div>';
  }
  if ($errors['limbs']) {
    setcookie('limbs_error', '', 1);
    $messages[] = '<div class="">Укажите количество конечностей.</div>';
  }
  if ($errors['biography']) {
    setcookie('biography_error', '', 1);
    $messages[] = '<div class="">Заполните биографию.</div>';
  }
  if ($errors['check']) {
    setcookie('check_error', '', 1);
    $messages[] = '<div class="">Согласитесь с отправкой</div>';
  }
  if (!empty($_COOKIE['login_check']) && empty($_SESSION['login'])) {    
    setcookie('login_check', '', 1);
    $messages['save'] = '<div>Email занят</div>';
  }
// Складываем предыдущие значения полей в массив, если есть.
$values = array();
$values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
$values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
$values['date'] = empty($_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];
$values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
$values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
$values['abilities'] = empty($_COOKIE['abilities_value']) ? '' : $_COOKIE['abilities_value'];
$values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];

  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  if (!empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])) {
    // TODO: загрузить данные пользователя из БД
   
    $stmt = $db->prepare("SELECT full_name FROM application WHERE email = ?");
    $stmt -> execute([$_SESSION['login']]); 
    $values['fio'] = $stmt->fetchColumn();
    
    $values['email'] = $_SESSION['login'];
    
    $stmt = $db->prepare("SELECT date FROM application WHERE email = ?");
    $stmt->execute([$_SESSION['login']]);
    $values['date'] = $stmt->fetchColumn();
    
    $stmt = $db->prepare("SELECT sex FROM application WHERE email = ?");
    $stmt->execute([$_SESSION['login']]);
    $values['sex'] = $stmt->fetchColumn();
    
    $stmt = $db->prepare("SELECT limbs FROM application WHERE email = ?");
    $stmt->execute([$_SESSION['login']]);
    $values['limbs'] = $stmt->fetchColumn();
    
    $stmt = $db->prepare("SELECT abilities FROM application WHERE email = ?");
    $stmt->execute([$_SESSION['login']]);
    $values['abilities'] = $stmt->fetchColumn();
    
    $stmt = $db->prepare("SELECT biography FROM application WHERE email = ?");
    $stmt->execute([$_SESSION['login']]);
    $values['biography'] = $stmt->fetchColumn();
  }
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (!preg_match("#^[А-Яа-яЁё]+\s[А-Яа-яЁё]+\s[А-Яа-яЁё]+$#u", $_POST['fio'])) {
      setcookie('fio_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
  }
  if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
      setcookie('email_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['date'])) {
      setcookie('date_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['sex'])) {
      setcookie('sex_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['limbs'])) {
      setcookie('limbs_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['biography'])) {
      setcookie('biography_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
  }
  setcookie('abilities_value', serialize($_POST['abilities']), time() + 30 * 24 * 60 * 60);
  if (empty($_POST['check'])) {
      setcookie('check_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      setcookie('check_value', $_POST['check'], time() + 30 * 24 * 60 * 60);
  }
  
  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 1);
    setcookie('fio_value', '', 1);
    setcookie('email_error', '', 1);
    setcookie('email_value', '', 1);
    setcookie('date_error', '', 1);
    setcookie('date_value', '', 1);
    setcookie('sex_error', '', 1);
    setcookie('sex_value', '', 1);
    setcookie('limbs_error', '', 1);
    setcookie('limbs_value', '', 1);
    setcookie('biography_error', '', 1);
    setcookie('biography_value', '', 1);
    setcookie('check_error', '', 1);
    setcookie('check_value', '', 1);
    setcookie('abilities_value', '', 1);
  }

           
    switch($_POST['sex']) {
      case 'm': {
          $sex='male';
          break;
      }
      case 'f':{
          $sex='female';
          break;
      }
    };
    
    switch($_POST['limbs']) {
        case '1': {
            $limbs='1';
            break;
        }
        case '2':{
            $limbs='2';
            break;
        }
        case '3':{
            $limbs='3';
            break;
        }
        case '4':{
            $limbs='4';
            break;
        }
    };
          
    $abilities = serialize($_POST['abilities']);



  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
      // Обновляем данные пользователя по email
      try {      
                
        $stmt = $db->prepare("UPDATE application SET full_name = ?, date = ?, sex = ?, limbs = ?, abilities = ?, biography = ?");
        $stmt -> execute(array($_POST['fio'],$_POST['date'],$sex,intval($limbs),$abilities,$_POST['biography']));
      }
      catch(PDOException $e){
          print('Error : ' . $e->getMessage());
          exit();
      }
  }
  else {
    // Проверяем email на уникальность
    try {
      $stmt = $db->prepare("SELECT email FROM application WHERE email = ?");
      $stmt -> execute([$_POST['email']]);
      $log_check = $stmt->fetchColumn();
      }
      catch(PDOException $e){
          print('Error : ' . $e->getMessage());
          exit();
      }
      if ($log_check != NULL){
          setcookie('login_check', '1');
      } else {
          // Генерируем уникальный логин и пароль.
          // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
          $login = $_POST['email'];
          $pass = substr(uniqid(), 0, 8);
          // Сохраняем в Cookies.
          setcookie('login', $login);
          setcookie('pass', $pass);

          $password_md5 = md5($pass);
          try {
              // print_r(array($_POST['fio'],$_POST['email'],$_POST['year'],$sex,intval($limbs),$abilities,$_POST['biography'],$password1));
              // strlen($password1);
              $stmt = $db->prepare("INSERT INTO application SET full_name = ?, email = ?, date = ?, sex = ?, limbs = ?, abilities = ?, biography = ?, password = ?");
              $stmt -> execute(array($_POST['fio'],$_POST['email'],$_POST['date'],$sex,intval($limbs),$abilities,$_POST['biography'],$password_md5));
          }
          catch(PDOException $e){
              print('Error : ' . $e->getMessage());
              exit();
          }
      }
  }

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // // Делаем перенаправление.
  header('Location: ./');
}
